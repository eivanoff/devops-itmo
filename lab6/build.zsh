#!/bin/zsh

set -xe

INIT_PWD=$(pwd)

dirs=('backend' 'nginx-proxy')

for dir in "${dirs[@]}"; do
  if [ -d "$dir" ]; then
    cd "$dir" || exit
    for file in *.zsh; do
      zsh "$file"
    done
    cd "$INIT_PWD" || exit
  else
    echo "Directory '$dir' does not exist."
  fi
done
